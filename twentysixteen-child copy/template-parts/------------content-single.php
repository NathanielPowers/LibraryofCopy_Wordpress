<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<!--		< ?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>-->
  
      <div class="row">
        <div class="col-sm-12 col-lg-4">
          <!-- container for categories (tagline, body copy, source) -->
          
          <div class="categoryContainer">
            <p class="categoryHeader">tagline</p>
          </div>
          
          <h1 class="entry-title"><?php the_field('tagline'); ?></h1>
          
        </div>
        <div class="col-sm-12 col-lg-5">
          
          <div class="categoryContainer">
            <p class="categoryHeader">body copy</p>
          </div>
          
          <h2 class="bodyCopy"><?php the_field('body_copy'); ?></h2>
          
          <div class="categoryContainer">
            <p class="categoryHeader">tags</p>
          </div>
            <h3 class="tags">
              <?php the_tags( '', ', ', '<br />' ); ?>
            </h3>
          <div class="categoryContainer">
            <p class="categoryHeader">source</p>
          </div>
          
          <h4 class="year"><?php the_field('yearLabel'); ?> <?php the_field('year'); ?></h4>

          <h5 class="meta"><?php the_field('meta'); ?></h5>
          
        </div>
        
        <div class="col-sm-12 col-lg-3">
          
          <div class="categoryContainer">
            <p class="categoryHeader">image</p>
          </div>
          
          
          <img src="<?php the_field('ad_image'); ?>" />
          
        </div>
      </div>
    
<!--	< ?php twentysixteen_excerpt(); ?>-->

	<?php twentysixteen_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

//			if ( '' !== get_the_author_meta( 'description' ) ) {
//				get_template_part( 'template-parts/biography' );
//			}
		?>
	</div><!-- .entry-content -->

<!--
	< footer class="entry-footer">
		< ?php twentysixteen_entry_meta(); ?>
		< ?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer>
-->
</article><!-- #post-## -->
