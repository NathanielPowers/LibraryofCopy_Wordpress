<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
      
      <!-- Search and Tag Areas -->
      <section id="searchAndTags">
      <!-- search area -->
      <form id="search" role="search" method="get" class="search-form" action="https://libraryofcopy.com/">
        <div class="searchIconField"><svg class="search-icon" height="25" width="25" viewBox="0 0 39.7 39.7" aria-label="search" role="img"><title>search</title><path d="M31,28h-1.6l-0.6-0.5c2-2.3,3.1-5.2,3.1-8.5c0-7.2-5.8-13-13-13S6,11.8,6,19s5.8,13,13,13
          c3.2,0,6.2-1.2,8.5-3.1l0.5,0.6V31l10,10l3-3L31,28z M19,28c-5,0-9-4-9-9s4-9,9-9s9,4,9,9S24,28,19,28z"></path></svg>
          <label>
            <span class="screen-reader-text">Search for:</span>
            <input class="search-field" placeholder="search" value="" name="s" type="search">
          </label>
          <button type="submit" class="search-submit"><span class="screen-reader-text">Search</span></button>
        </div>
      </form>  


      <!-- tag categories -->
      <div class="categoryList">
        <!--
<ul>
< ?php wp_list_categories(); ?> 
</ul>
-->
        <ul>
          <?php wp_list_categories( array(
  'exclude'  => array(),
  'title_li' => ''
) ); ?>
        </ul>
      </div>
      </section>

	</main><!-- .site-main -->

<!--	< ?php get_sidebar( 'content-bottom' ); ?>-->

</div><!-- .content-area -->

<!--< ?php get_sidebar(); ?>-->
<?php get_footer(); ?>
