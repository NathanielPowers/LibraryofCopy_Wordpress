<?php
function my_theme_enqueue_styles() {

  $parent_style = 'twentyfifteen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

  wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'child-style',
                   get_stylesheet_directory_uri() . '/style.css',
                   array( $parent_style ),
                   wp_get_theme()->get('Version')
                  );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function add_search_to_wp_menu ( $items, $args ) {
  if( 'primary' === $args -> theme_location ) {
    $items .= '<li class="menu-item menu-item-search">';
    $items .= '<button class="searchBtn"><i class="fa fa-search"></i></button>
    ';

    $items .= '</li>';
  }
  return $items;
}
add_filter('wp_nav_menu_items','add_search_to_wp_menu',10,2);

?>

