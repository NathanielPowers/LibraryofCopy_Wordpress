"use strict";

var gulp = require('gulp'),
//    concat = require('gulp-concat'),
//    uglify = require('gulp-uglify'),
//    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    maps = require('gulp-sourcemaps'),
    del = require('del');


gulp.task('compileSass', function() {
  return gulp.src("scss/style.scss")
    .pipe(maps.init())
    .pipe(sass())
    .pipe(maps.write('./'))
    .pipe(gulp.dest('css'));
});

gulp.task('watchFiles', function() {
  gulp.watch('scss/**/*.scss', ['compileSass']);
})

gulp.task('clean', function() {
  del(['dist', 'css/style.css*', 'js/app*.js*']);
});

gulp.task("build", ['compileSass'], function() {
  return gulp.src(["css/style.css", 'index.html',
                   "img/**", "fonts/**"], { base: './'})
    .pipe(gulp.dest('dist'));
});

gulp.task('serve', ['watchFiles']);

gulp.task("default", ["clean"], function() {
  gulp.start('build');
});