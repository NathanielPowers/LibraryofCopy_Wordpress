<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<!--		< ?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>-->
  
      <div class="row">
        <div class="col-sm-12 col-lg-5">
          <!-- container for categories (tagline, body copy, source) -->
          
          <div class="categoryContainer">
            <p class="categoryHeader">tagline</p>
          
          <h1 class="entry-title"><?php the_field('tagline'); ?></h1>
          </div>

        </div>
        
        <div class="col-sm-12 col-lg-7">
          
          <div class="categoryContainer">
            <p class="categoryHeader">image</p>
          </div>
          
          
          <img class="adImage" src="<?php the_field('ad_image'); ?>" />
          
        </div>
        
  </div>
  <div class="row">
    <div class="col-sm-12 col-lg-5"></div>

        <div class="col-sm-12 col-lg-7">

          <div class="categoryContainer">
            <p class="categoryHeader">body copy</p>

            <h2 class="bodyCopy"><?php the_field('body_copy'); ?></h2>

          </div>
          
        </div>

  </div>
  <div class="row">
    <div class="col-sm-12 col-lg-5"></div>

        <div class="col-sm-12 col-lg-3">

          <div class="categoryContainer">
            <p class="categoryHeader">tags</p>
            <h3 class="tags">
              <?php the_tags( '', ', ', '<br />' ); ?>
            </h3>
          </div>
    </div>
    <div class="col-sm-12 col-lg-4">

          <div class="categoryContainer metaInfo">
            <p class="categoryHeader">meta</p>

            <h4 class="placement"><?php the_field('adPlacementLabel'); ?> <?php the_field('adPlacement'); ?></h4>

            <h5 class="year"><?php the_field('yearLabel'); ?> <?php the_field('year'); ?></h5>

            <h6 class="meta"><?php the_field('meta'); ?></h6>

          </div>
        </div>

      </div>
    
<!--	< ?php twentysixteen_excerpt(); ?>-->

	<?php twentysixteen_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

//			if ( '' !== get_the_author_meta( 'description' ) ) {
//				get_template_part( 'template-parts/biography' );
//			}
		?>
	</div><!-- .entry-content -->

<!--
	< footer class="entry-footer">
		< ?php twentysixteen_entry_meta(); ?>
		< ?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer>
-->
</article><!-- #post-## -->
