<?php get_header(); ?>
<div class="container">

<!--
<div class="blog-header">
        <h1 class="blog-title">< ?php bloginfo('name'); ?></h1>
        <p class="lead blog-description">< ?php bloginfo('description'); ?></p>
  <h1>INDEX</h1>
      </div>
-->
  
  <section id="homePage1">
    <div class="row">
      <div class="col-sm-12">
        <div class="homeHero">
          <!--          <img src="http://www.nathanielpowers.com/wp-content/uploads/2017/10/nathanielPowersLogo.png">-->
          <h1>Library of Copy</h1>
          <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
            <label>
              <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
              <input type="search" class="search-field"
                     placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
                     value="<?php echo get_search_query() ?>" name="s"
                     title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
            </label>
            <input type="submit" class="search-submit"
                   value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
          </form>

        </div>
      </div>
    </div>
  </section>

      <div class="row">
        <div class="col-sm-8 blog-main">
          <?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post(); ?>
            <div class="blog-post">
              <h2 class="blog-post-title">
                  <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                  </a>
              </h2>
              <p class="blog-post-meta">
                <?php the_time('F j, Y g:i a'); ?>
                by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
                <?php the_author(); ?>
                </a>
              </p>
              <?php the_excerpt(); ?>

            </div><!-- /.blog-post -->
<!--              < ?php get_template_part('content', get_post_format()); ? >-->
          <?php endwhile; ?>
        <?php else : ?>
          <p><?php __('No Posts Found'); ?></p>
        <?php endif; ?>
        </div><!-- /.blog-main -->
  </div>
</div>
<?php get_footer(); ?>
