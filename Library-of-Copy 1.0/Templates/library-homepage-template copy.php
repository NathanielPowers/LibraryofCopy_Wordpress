<?php

/*
Template Name: Library Homepage Template
*/


get_header();
?>

  <section id="homePage1">
    <div class="row">
      <div class="col-sm-12">
        <div class="homeHero">
<!--          <img src="http://www.nathanielpowers.com/wp-content/uploads/2017/10/nathanielPowersLogo.png">-->
          <h1>Library of Copy</h1>
          <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
            <label>
              <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
              <input type="search" class="search-field"
                     placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
                     value="<?php echo get_search_query() ?>" name="s"
                     title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
            </label>
            <input type="submit" class="search-submit"
                   value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
          </form>

        </div>
      </div>
    </div>
  </section>

  <section id="work">
    
    <?php if(have_posts()) : ?>
    <?php while(have_posts()) : the_post(); ?>
    <?php get_template_part('custom-content', get_post_format()); ?>
    <?php endwhile; ?>
    <?php else : ?>
    <p><?php __('No Posts Found'); ?></p>
    <?php endif; ?>


  </section>

  
  <?php 
get_footer();

?>
