<?php if(!is_front_page()) : ?>
  <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    <?php if(is_active_sidebar('sidebar')): ?>
      <?php dynamic_sidebar('sidebar'); ?>
    <?php endif; ?>
  </div><!-- /.blog-sidebar -->
<?php endif; ?>

<footer class="blog-footer">
  <a id="backToTop" href="#">Back to top</a>
  <p>&copy; <?php echo Date('Y'); ?> - <?php bloginfo('name'); ?></p>
</footer>
<?php wp_footer(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.js"></script>

<script>window.jQuery || document.write('<script src="<?php bloginfo('template_url'); ?>js/jquery.min.js"><\/script>')</script>
    <script src="<?php bloginfo('template_url'); ?>js/bootstrap.min.js"></script>


<script>$(document).ready(function() {
    $(".navbar-toggle").click(function() {
      $(".navbar").toggleClass("navActive");
//      $("#primaryNav").toggleClass("navZShow");
//      $("#headerImg").toggleClass("headerHeight");
//      $("#navMenuBtn, #navMenuClose").toggleClass("naveMenuBtn navMenuClose");

      $("#navMenuClose").show();
    });
    $("#navMenuClose").click(function() {
      $(".navbar").toggleClass("navActive");
//      $("#primaryNav").toggleClass("navZShow");
//      $("#headerImg").toggleClass("headerHeight");
//      $("#navMenuBtn, #navMenuClose").toggleClass("naveMenuBtn navMenuClose");
//      $(".close-button").hide();
    });
  });</script>


</body>
</html>
