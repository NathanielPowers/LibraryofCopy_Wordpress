<?php

/*
Template Name: Library of Copy Hompage
*/


get_header();
?>
  <div class="container">

    <!--
<div class="blog-header">
<h1 class="blog-title">< ?php bloginfo('name'); ?></h1>
<p class="lead blog-description">< ?php bloginfo('description'); ?></p>
<h1>INDEX</h1>
</div>
-->

    <section id="homePage1">
      <div class="row">
        <div class="col-sm-12">
          <div class="homeHero">
            <!--          <img src="http://www.nathanielpowers.com/wp-content/uploads/2017/10/nathanielPowersLogo.png">-->
<!--            <h1>Library of Copy</h1>-->


            <form role="search" method="get" class="search-form" action="https://libraryofcopy.com/">
              <label>
      <span class="screen-reader-text">Search for:</span>
      <input class="search-field" placeholder="Search …" value="" name="s" type="search">
    </label>
              <button type="submit" class="search-submit"><span class="screen-reader-text">Search</span></button>
            </form>


          </div>
        </div>
      </div>
    </section>

    <div class="row">
      <div class="col-sm-12">

        <div id="primary" class="content-area-full-width">
          <main id="main" class="site-main" role="main">
            <?php
      // Start the loop.
      while ( have_posts() ) : the_post();

      // Include the page content template.
      get_template_part( 'template-parts/content', 'page' );

      // If comments are open or we have at least one comment, load up the comment template.
      //if ( comments_open() || get_comments_number() ) {
      //  comments_template();
      //}

      // End of the loop.
      endwhile;
      ?>

          </main>
          <!-- .site-main -->

<!--          <?php get_sidebar( 'content-bottom' ); ?>-->

        </div>
        <!-- .content-area -->
      </div>
    </div>
  </div>
  <?php get_footer(); ?>
