<?php
function my_theme_enqueue_styles() {

  $parent_style = 'twentyfifteen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

  wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'child-style',
                   get_stylesheet_directory_uri() . '/style.css',
                   array( $parent_style ),
                   wp_get_theme()->get('Version')
                  );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
?>


<!-- add search wigit -->
register_sidebar( array(
'name' => __( 'Your Widget Area', 'twentysixteen-child' ),
'id' => 'my-widget-area',
'description' => __( 'The primary widget area', 'twentysixteen-child' ),
'before_widget' => '
<ul>
  <li id="%1$s" class="widget-container %2$s">',
    'after_widget' => '</li>
</ul>
',
'before_title' => '
<h3 class="widget-title">',
  'after_title' => '</h3>
',
) );